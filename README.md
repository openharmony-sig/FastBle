# FastBle

## Introduction

FastBle is a library that can filter, scan, connect, read, and write BLE devices.

![gif](preview/operation_EN.gif)

## How to Install

```shell
ohpm install @ohos/fastble 
```

For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use

1. Initialize the module.

```
BleManager.getInstance().init();
```

2. Carry out initialization configuration.

```
BleManager.getInstance()
            .enableLog(true)
            .setReConnectCount(1, 5000)
            .setConnectOverTime(20000)
            .setOperateTimeout(5000);
```

3. Configure scanning rules.

```
let scanRuleConfig: BleScanRuleConfig = new BleScanRuleConfig.Builder()
              .setServiceUuids(serviceUuids)      // (Optional) Scan only devices of the specified service.
              .setDeviceName(true, names)         // (Optional) Scan only devices with specified broadcast names.
              .setDeviceMac(mac)                  // (Optional) Scan only devices with specified MAC addresses.
              .setAutoConnect(isAutoConnect)      // (Optional) Set automatic connection. The default value is false.
              .setScanTimeOut(10000)              // (Optional) Set the scanning timeout interval. The default value is 10 seconds.
              .build();
BleManager.getInstance().initScanRule(scanRuleConfig);
```

4. Scan the device.

```
   let _this = this;
    class TempBleScanCallback extends BleScanCallback {
      onScanStarted(success: boolean): void {
        console.info("onScanStarted success:" + success);
        _this.clearScanDevice();
        _this.is_loading = true;
        _this.loading_rotate = 360;
        _this.btn_scan_text = $r('app.string.stop_scan');
        _this.connectedDevices = BleManager.getInstance().getAllConnectedDevice();

      }

      onLeScan(bleDevice: BleDevice): void {
        console.info("onLeScan");
      }

      onScanning(bleDevice: BleDevice): void {
        console.info("onScanning");
        ArrayHelper.add(_this.bleDeviceList, bleDevice);

      }

      onScanFinished(scanResultList: Array<BleDevice>): void {

        console.info("onScanFinished");
        _this.is_loading = false;
        _this.loading_rotate = 0;
        _this.btn_scan_text = $r('app.string.start_scan');
        _this.connectedDevices = BleManager.getInstance().getAllConnectedDevice();
      }
    }

    BleManager.getInstance().scan(new TempBleScanCallback());
```

5. Connect to the device.

```
   let _this = this;

    class IndexBleGattCallback extends BleGattCallback {
      public onStartConnect(): void {
        _this.progressDialogCtrl.open();
      }

      public onConnectFail(bleDevice: BleDevice, exception: BleException): void {

        _this.progressDialogCtrl.close();
        prompt.showToast({ message: _this.toast_connect_fail, duration: 300, })
      }

      public onConnectSuccess(bleDevice: BleDevice, gatt: ble.GattClientDevice, status: number): void {
        _this.progressDialogCtrl.close();
        ArrayHelper.add(_this.connectedDevices, bleDevice.getMac());
      }

      public onDisConnected(isActiveDisConnected: boolean, device: BleDevice, gatt: ble.GattClientDevice, status: number): void {

        _this.progressDialogCtrl.close();
        _this.connectedDevices = BleManager.getInstance().getAllConnectedDevice();

        if (isActiveDisConnected) {
          prompt.showToast({ message: _this.toast_active_disconnected, duration: 300 })
        } else {
          prompt.showToast({ message: _this.toast_disconnected, duration: 300 })
        }
      }
    }

    BleManager.getInstance().connect(bleDevice, new IndexBleGattCallback());
```

6. Cancel scanning.

```
BleManager.getInstance().cancelScan();
```

7. Read data.

```
            let bleReadClass: BleReadCallback = new BleReadClass()

            BleManager.getInstance().read(
              this.device,
              this.characteristic.serviceUuid,
              this.characteristic.characteristicUuid,
              bleReadClass);
```

8. Write data.

```
            let tempBleWriteCallback: BleWriteCallback = new TempBleWriteCallback()

            BleManager.getInstance().write(
              this.device,
              this.characteristic.serviceUuid,
              this.characteristic.characteristicUuid,
              HexUtil.hexStringToBytes(hex),
              true,
              true,
              0,
              tempBleWriteCallback);
          
```

## Available APIs

1. Obtains an instance.
   `public static getInstance(): BleManager;`
2. Configures scanning rules.
   `public initScanRule(config: BleScanRuleConfig)`
3. Scans the device.
   `public scan(callback: BleScanCallback)`
4. Cancels scanning.
   `public cancelScan()`
5. Connects to the device.
   `public connect(device: BleDevice | string, bleGattCallback: BleGattCallback)`
6. Checks whether the device is connected.
   `public isConnected(device: BleDevice | string): boolean`
7. Disconnects from the device.
   `public disconnect(bleDevice: BleDevice)`
8. Reads data.
   `public read(bleDevice: BleDevice, uuid_service: string, uuid_read: string, callback: BleReadCallback)`
9. Writes data.
   `public write(bleDevice: BleDevice, uuid_service: string, uuid_write: string, data: Uint8Array, split: boolean=true, sendNextWhenLastSuccess: boolean=true, intervalBetweenTwoPackage: number=0, callback: BleWriteCallback)`

## Constraints

- DevEco Studio: 4.1 Canary (4.1.3.317)

- OpenHarmony SDK: API 11 (4.1.0.36)

## Directory Structure

````
|---- FastBle
|     |---- entry  # Sample code
|     |---- wxFastBle  # FastBle library
|	    |----src
          |----main
              |----ets
                  |----Bluetooth core logic
                  |----callback # Callback processing
                  |----exception # Exception handling
                  |----scan # Bluetooth scanning implementation
                  |----data # Data processing implementation
                  |----utils # Tools
                  |----BleManager.ets # Bluetooth connection management
|           |---- index.ets  # External APIs
|     |---- README.md  # Readme                   
````

## How to Contribute

If you find any problem when using the project, submit an [issue](https://gitee.com/openharmony-sig/FastBle/issues) or a [PR](https://gitee.com/openharmony-sig/FastBle/pulls).


## License

This project is licensed under [Apache LICENSE](https://gitee.com/openharmony-sig/FastBle/blob/master/LICENSE).
