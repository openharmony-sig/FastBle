## v2.0.5-rc.0

- resolving the issue where the BleManager.getInstance().write() method fails when writing large amounts of data with the data segmentation set to true.。

## v2.0.4

- 发布2.0.4正式版

## v2.0.3

- 修改调用setMtu时返回设置失败问题。
- 修改调用write进行特征值写入时，部分设备收不到数据问题。
- 修改调用write方法后，关闭手机蓝牙，然后打开重新扫描链接后再调用write方法，onCharacteristicChanged会多次回调。
- 优化蓝牙扫描、连接、断开连接、以及监听事件注册的逻辑，添加异常捕获。

## v2.0.2

- 修改调用BleManager的notify接口后，在onNotifySuccess回调中再调用BleManager的write方法后，报错device is busy的BUG。

## v2.0.1

- 新增notify特征值改变事件监听

## v2.0.1-rc.0

- 完善权限配置字段
- 修复语法报错问题

## v2.0.0

- 适配DevEco Studio 版本：4.1 Canary(4.1.3.317)，OpenHarmony SDK:API11 (4.1.0.36)
- ArkTs语法适配

## v1.1.0

- 名称由@ohos/fastble-ets修改为@ohos/fastble
- 旧的包@ohos/fastble-ets已不维护，请使用新包@ohos/fastble

## v1.0.1

- api8升级到api9 stage模型
- 解决一些bug，如写操作的crash问题，解析权限处理流
- 修改描述符操作，为搜索结果添加设备名称

## v1.0.0

- 已实现功能
  1.初始化
  2.扫描
  3.连接
  4.取消扫描
  5.打开蓝牙
  6.关闭蓝牙
  7.读Characteristic
  8.写Characteristic